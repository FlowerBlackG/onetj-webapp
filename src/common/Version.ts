/* 上财果团团 */

export default class Version {

    static code = 3
    static tag = "1.0.0-rc3"

    /**
     * 构建时间。遵循 ISO8601 格式。
     */
    static buildTime = '2024-01-11T22:51+08:00'

    private constructor() {}
}
